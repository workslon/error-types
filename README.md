# Overview

Nodejs module - adapted version of [`errorTypes.js` library](https://bitbucket.org/workslon/ontojs/src/8367a54d7dbdc4330a750dfdfbbd9a3e27e17867/lib/errorTypes.js?at=master&fileviewer=file-view-default) written by Prof. Gerd Wagner, BTU Cottbus

You can find its source code [here](https://bitbucket.org/workslon/error-types/src/f4fdde6bc0b466fcc3aef12b84ca7cb1a939e08e/index.js)

# Install

## ssh

```
npm install git+ssh://git@bitbucket.org/workslon/error-types.git --save
```

## https

```
npm install https://git@bitbucket.org/workslon/error-types.git --save
```

# Usage

## CommonJS

```javascript
var errorTypes = require('error-types');
var RangeConstraintViolation = errorTypes.RangeConstraintViolation;

// ...

```

## ES6 Modules

### Load The Whole Library

```javascript
import errorTypes from 'error-types';
```

### Get Required Types Only

```javascript
import { RangeConstraintViolation, ViewConstraintViolation } from 'error-types';
```