'use strict';
/**
 * @fileOverview  Defines error classes (also called "exception" classes)
 * @author Gerd Wagner
 */

function ConstraintViolation( msg, culprit) {
  this.message = msg;
  if (culprit) this.culprit = culprit;
}
function NoConstraintViolation( v) {
  if (v !== undefined) this.checkedValue = v;
  this.message = "";
}
NoConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
NoConstraintViolation.prototype.constructor = NoConstraintViolation;

/*
 * Property Constraint Violations
 */
function MandatoryValueConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
MandatoryValueConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
MandatoryValueConstraintViolation.prototype.constructor = MandatoryValueConstraintViolation;

function RangeConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
RangeConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
RangeConstraintViolation.prototype.constructor = RangeConstraintViolation;

function StringLengthConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
StringLengthConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
StringLengthConstraintViolation.prototype.constructor = StringLengthConstraintViolation;

function IntervalConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
IntervalConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
IntervalConstraintViolation.prototype.constructor = IntervalConstraintViolation;

function PatternConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
PatternConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
PatternConstraintViolation.prototype.constructor = PatternConstraintViolation;

function UniquenessConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
UniquenessConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
UniquenessConstraintViolation.prototype.constructor = UniquenessConstraintViolation;

function CardinalityConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
CardinalityConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
CardinalityConstraintViolation.prototype.constructor = CardinalityConstraintViolation;

function ReferentialIntegrityConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
};
ReferentialIntegrityConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
ReferentialIntegrityConstraintViolation.prototype.constructor = ReferentialIntegrityConstraintViolation;

function FrozenValueConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
};
FrozenValueConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
FrozenValueConstraintViolation.prototype.constructor = FrozenValueConstraintViolation;

/*
 * Entity Type Constraint Violations
 */
function cOMPLEXtYPEconstraintViolation(msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
cOMPLEXtYPEconstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
cOMPLEXtYPEconstraintViolation.prototype.constructor = cOMPLEXtYPEconstraintViolation;

function cOMPLEXdATAtYPEconstraintViolation(msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
cOMPLEXdATAtYPEconstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
cOMPLEXdATAtYPEconstraintViolation.prototype.constructor = cOMPLEXdATAtYPEconstraintViolation;

function eNTITYtYPEconstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
eNTITYtYPEconstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
eNTITYtYPEconstraintViolation.prototype.constructor = eNTITYtYPEconstraintViolation;

function ModelClassConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
ModelClassConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
ModelClassConstraintViolation.prototype.constructor = ModelClassConstraintViolation;

function ViewConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
ViewConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
ViewConstraintViolation.prototype.constructor = ViewConstraintViolation;

function ObjectTypeConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
ObjectTypeConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
ObjectTypeConstraintViolation.prototype.constructor = ObjectTypeConstraintViolation;

function AgentTypeConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
AgentTypeConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
AgentTypeConstraintViolation.prototype.constructor = AgentTypeConstraintViolation;

function KindConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
KindConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
KindConstraintViolation.prototype.constructor = KindConstraintViolation;

function RoleConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
RoleConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
RoleConstraintViolation.prototype.constructor = RoleConstraintViolation;

module.exports = {
  ConstraintViolation: ConstraintViolation,
  NoConstraintViolation: NoConstraintViolation,
  MandatoryValueConstraintViolation: MandatoryValueConstraintViolation,
  RangeConstraintViolation: RangeConstraintViolation,
  StringLengthConstraintViolation: StringLengthConstraintViolation,
  IntervalConstraintViolation: IntervalConstraintViolation,
  PatternConstraintViolation: PatternConstraintViolation,
  UniquenessConstraintViolation: UniquenessConstraintViolation,
  CardinalityConstraintViolation: CardinalityConstraintViolation,
  ReferentialIntegrityConstraintViolation: ReferentialIntegrityConstraintViolation,
  FrozenValueConstraintViolation: FrozenValueConstraintViolation,
  cOMPLEXtYPEconstraintViolation: cOMPLEXtYPEconstraintViolation,
  cOMPLEXdATAtYPEconstraintViolation: cOMPLEXdATAtYPEconstraintViolation,
  eNTITYtYPEconstraintViolation: eNTITYtYPEconstraintViolation,
  ModelClassConstraintViolation: ModelClassConstraintViolation,
  ViewConstraintViolation: ViewConstraintViolation,
  ObjectTypeConstraintViolation: ObjectTypeConstraintViolation,
  AgentTypeConstraintViolation: AgentTypeConstraintViolation,
  KindConstraintViolation: KindConstraintViolation,
  RoleConstraintViolation: RoleConstraintViolation
};